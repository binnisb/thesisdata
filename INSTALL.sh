MINICONDA=Miniconda-latest-Linux-x86_64.sh
wget http://repo.continuum.io/miniconda/$MINICONDA
chmod u+x $MINICONDA
pref=$(pwd)/miniconda 
./$MINICONDA -b -p $pref
export PATH="$pref/bin:$PATH"
echo $PATH
conda create -n sci2 python=2
conda create -n sci3 python=3

source activate sci2
conda install pip argcomplete numpy scipy scikit-learn pandas ipython-notebook matplotlib binstar
pip install ipdb
source deactivate

source activate sci3
conda install pip argcomplete numpy scipy scikit-learn pandas ipython-notebook matplotlib binstar
pip install ipdb
pip install snakemake

