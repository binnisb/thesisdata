import matplotlib
matplotlib.use('Agg')
import sys
from glob import glob
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from random import sample, seed
from sklearn import metrics
from itertools import product
from collections import namedtuple, defaultdict
from distutils.version import LooseVersion

def string_to_int(s):
    """Create a unique integer from a string
    """
    ord3 = lambda x : '%.3d' % ord(x)
    return int(''.join(map(ord3, s)))

def all_dict():

    datasets = ["Strain","Species"]
    thresholds = list(map(str,matplotlib.cbook.flatten([1, range(500,10001,500)])))
    covariances = ["spherical","diag","tied","full"]
    pcas = list(map(str,range(70,101,1)))
    samples = [list(map(str,range(2,65,2))),list(map(str,range(2,97,2)))]
    final = ["Fixed","Guess"]
    dirichlet = ["0.01", "0.1", "1", "10", "100"]
    experiments = ["PCA","Covariance","Threshold","Samples","Final","Fcomp", "Dirichlet", "Dcomp"]
    repeats = list(map(str,range(5)))

    thr = list(product(datasets,product(thresholds,repeats)))
    cov = list(product(datasets,product(covariances,repeats)))
    pca = list(product(datasets,product(pcas,repeats)))
    sam_1 = list(product([datasets[0]],product(samples[0],repeats)))
    sam_2 = list(product([datasets[1]],product(samples[1],repeats)))
    sam = sam_1 + sam_2
    fin = list(product(datasets,product(final,repeats)))
    dpfin = list(product(datasets,product(dirichlet,repeats[:1])))

    t = list(product(["Threshold"],thr))
    c = list(product(["Covariance"],cov))
    p = list(product(["Pca"],pca))
    s = list(product(["Samples"],sam_1+sam_2))
    f = list(product(["Final"],fin))
    f2 = list(product(["Fcomp"],fin))
    d = list(product(["Dirichlet"],dpfin))
    d2 = list(product(["Dcomp"],dpfin))
    all_dirs = c+t+p+s+f+f2+d+d2

    exd = defaultdict(lambda : defaultdict(lambda : defaultdict(list)))
    for exp,(ds,(ty,rep)) in all_dirs:
        # use i as seed for concoct
        hash('asdf') % ((sys.maxsize + 1) * 2)
        i = (hash(string_to_int("{}{}{}{}".format(exp,ds,ty,rep))) % (4294967294)) + 1
        exd[exp][ds][ty].append(i)
    return experiments, datasets, thresholds, covariances, pcas, samples, final, dirichlet, exd

def mkdir(path_to_dir):
    """Create a directory if it does not exist, otherwise do nothing"""
    if not os.path.isdir(path_to_dir):
        os.makedirs(path_to_dir)

def seaborn_plot(df,col,plot_label):
    sns.axes_style("darkgrid")
    sns.set_context("poster", font_scale=1.2)
    fig_size = 7
    def plot_exp(exp,ylabel,sharey=True, hue=None):
        g = sns.factorplot(x="Type",
                           y=exp[0],
                           hue="Metric" if len(exp) == 2 else None,
                           col=col,
                           data=df,
                           size=fig_size,
                           legend_out=False,
                           sharex=True,
                           sharey=sharey,
                           col_order=["Strain","Species"],
                           order=plot_label.order)
        if sharey:
            g.set(ylim=(0,g.axes.flat[0].get_ylim()[1]))
        g.set_ylabels(ylabel)
        g.set_xticklabels(plot_label.labels)
        g.set_xticklabels(rotation=45)
        for ax,xlabel in zip(g.axes.flat,plot_label.xlabels):
            ax.set_xlabel(xlabel)
        return g

    g1 = plot_exp(["Value","Metric"],"",hue=None)
    g1.set(ylim=(0,1))
    g1.add_legend()

    g2 = plot_exp(["Minutes"],"Execution in minutes",False)

    g3 = plot_exp(["PCA"],"# PCA components")

    g4 = plot_exp(["PassThreshold"],"# Contigs for fitting")

    return g1,g2,g3,g4

def get_max_ari(df):
    #Finding the seed that gives the highest ARI:
    ## Select ARIs
    df_aris = df[df.Metric == "Adjusted Rand Index"]
    df_aris_grouped = df_aris.groupby(["Experiment","Dataset","Type"])
    ## Find the index of the highest ARI Value in the group
    df_idxmax_ari = df_aris_grouped.Value.idxmax("index")
    ## Get the seeds for the highest ARI Values
    seeds = df.Seed.iloc[df_idxmax_ari].values
    ## Filter DataFrame on the seeds
    df_seeds_filtered = df[df.Seed.isin(seeds)]
    return df_seeds_filtered

experiments, datasets, thresholds, covariances, pcas, samples, final, dirichlet, hierarchy_dict = all_dict()

cluster_range = {"Strain":"20,20,1","Species":"101,101,1"}
PlotLabel = namedtuple("PlotLabel",["labels","order","xlabels","xlim"])
labels = ([s if (s%10==0 or s in [2,64,96]) else '' for i,s in enumerate(map(int,samples[1]))],
          [s if i%2==0 else '' for i,s in enumerate(thresholds)],
          [s if i%3==0 else '' for i,s in enumerate(pcas)],
          ["Fixed","BIC"],
          ["Fixed","BIC"],
          ["Fixed","BIC"] + list(map(lambda x: "DP_{0}".format(x), dirichlet)))

plot_labels = {"Covariance": PlotLabel(covariances,covariances,["Covariance type"]*2,None),
               "Final":      PlotLabel(labels[3],final,["Final"]*2,None),
               "Fcomp":      PlotLabel(labels[4],final,["Fcomp"]*2,None),
               "Samples":    PlotLabel(labels[0],samples[1],["Number of samples", "Number of samples"],(2,96)),
               "Threshold":  PlotLabel(labels[1],thresholds,["Contig length threshold"]*2,None),
               "Pca":        PlotLabel(labels[2],pcas,["% Variance explained"]*2,None),
               "Dirichlet":    PlotLabel(dirichlet,dirichlet,["Alpha value"]*2,None),
               "Dcomp":    PlotLabel(dirichlet,dirichlet,["Alpha value"]*2,None),
               "Total_cov" : PlotLabel(labels[5],final + dirichlet,["Method"]*2,None),
               "Total_full" : PlotLabel(labels[5],final + dirichlet,["Method"]*2,None),
              }
plot_order = {"Covariance": covariances,}

exp = list(map(lambda x: x.lower(),experiments))

#ruleorder: plot_type_metrics > dirichlet > dpfinal_composition > final > final_composition
ruleorder: plot_type_metrics
tex = ["Final","Fcomp","Dirichlet","Dcomp"]
rule all:
    input:
        "all_data.csv",
        "count_clusters.csv",
        #"Total.csv",
        #"Total.tex",
        expand("{pre}_clusters.tex",pre=tex),
        expand("{pre}_metrics.tex",pre=tex),
        expand("complete_pre_rec_adjrand_{exp}.png",exp=exp),
        expand("pca_components_{exp}.png",exp=exp),
        expand("execution_time_{exp}.png",exp=exp),
        expand("contigs_pass_threshold_{exp}.png",exp=exp),
        expand("Total_{t}.png",t=["cov","full"])


rule total:
    input:
        "Total_cov.csv",
        "Total_full.csv",
    output:
        total = "Total.csv",
        tex = "Total.tex",
        clust = "Total_clust.tex"
    run:
        dfs = [pd.read_csv(f,header=0,dtype={"Type":str}) for f in input]
        df = pd.concat(dfs,ignore_index=True)
        df.to_csv(output.total,index=False,float_format="%.6f")

        df_group = df.iloc[:,1:].groupby([df.Experiment,df.Dataset,df.Type,df.Metric]).max()
        df_clust = df.iloc[:,1:].groupby([df.Experiment,df.Dataset,df.Type]).max()

        df_group = df_group.unstack("Experiment")
        df_group.columns = df_group.columns.droplevel()
        df_clust = df_clust.unstack("Experiment")
        df_clust.columns = df_clust.columns.droplevel()

        df_group.to_latex(output.tex,column_format="llllr")
        df_clust[["ClusterCount"]].to_latex(output.clust,column_format="lllr")


rule total_exp:
    input:
        ad = "all_data_max_ari.csv"
    output:
        plot = "Total_{t}.png",
        total = "Total_{t}.csv",
        tex_species = "Total_{t}_species.tex",
        tex_strain = "Total_{t}_strain.tex",

        cluster_tex = "Total_{t}_clusters.tex"
    run:
        ad = pd.read_csv(input.ad)

        f = ["Final","Dirichlet"] if wildcards.t == "cov" else ["Fcomp","Dcomp"]

        ad_gr = ad[((ad.Experiment == f[0]) | (ad.Experiment == f[1]))]

        ad_gr.Experiment = output.plot.split(".")[0]

        g1,g2,g3,g4 = seaborn_plot(ad_gr,"Dataset",plot_labels.get(output.plot.split(".")[0]))

        g1.savefig(output.plot,bbox_inches='tight')
        ad_gr.to_csv(output.total,index=False,float_format="%.6f")
        def map_names(x):
            if x == "Guess":
                return "BIC"
            elif x == "Fixed":
                return x
            else:
                return "DP_"+str(x)
        ad_gr.Type = ad_gr.Type.apply(lambda x: map_names(x))

        ad_gr_value = ad_gr.iloc[:,1:].groupby([ad_gr.Experiment,ad_gr.Dataset,ad_gr.Type,ad_gr.Metric]).max()
        ad_gr_value[(ad_gr_value.Dataset == "Species")][["Value"]].to_latex(output.tex_species,column_format="llllr")
        ad_gr_value[(ad_gr_value.Dataset == "Strain")][["Value"]].to_latex(output.tex_strain,column_format="llllr")
        ad_gr_clust = ad_gr.iloc[:,1:].groupby([ad_gr.Experiment,ad_gr.Dataset,ad_gr.Type]).max()
        ad_gr_clust[["ClusterCount"]].to_latex(output.cluster_tex,column_format="lllr")

rule cluster_tex:
    input:
        ad = "all_data_max_ari.csv"
    output:
        tex = "{pre}_clusters.tex"
    run:
        ad = pd.read_csv(input.ad)
        ad_gr = ad[((ad.Metric == "Adjusted Rand Index") & (ad.Experiment == wildcards.pre))].groupby([ad.Experiment,ad.Dataset,ad.Type]).max()
        ad_gr[["ClusterCount"]].to_latex(output.tex,column_format="lllr")

rule metrics_tex:
    input:
        ad = "all_data_max_ari.csv"
    output:
        tex = "{pre}_metrics.tex"
    run:
        ad = pd.read_csv(input.ad)
        ad_gr = ad[(ad.Experiment == wildcards.pre)].groupby([ad.Experiment,ad.Dataset,ad.Type,ad.Metric]).max()
        ad_gr[["Value"]].to_latex(output.tex,column_format="llllr")

rule count_clusters:
    input:
        cc = "count_clusters.sh",
        ad = "all_data_max_ari.csv"
    output:
        cc = "count_clusters.csv"
    shell:
        """./{input.cc} {input.ad} {output.cc}"""

rule all_data:
    input:
        data_csv = lambda wildcards: ["{}/complete_pre_rec_adjrand_{}.csv".format(e.capitalize(),e) for e in exp],
    output:
        df_csv = "all_data.csv",
        df_max_ari_csv = "all_data_max_ari.csv",
    run:
        dfs = [pd.read_csv(f,header=0,dtype={"Type":str}) for f in sorted(input.data_csv,key=LooseVersion)]
        df = pd.concat(dfs,ignore_index=True)

        df_max_ari = get_max_ari(df)

        df.to_csv(output.df_csv,index=False,float_format="%.6f")
        df_max_ari.to_csv(output.df_max_ari_csv,index=False,float_format="%.6f")


rule symlink:
    input:
        metric = lambda wildcards: "{}/{}_{}.png".format(str(wildcards.exp).capitalize(),wildcards.fig, wildcards.exp),

    output:
        metric = "{fig,\w+}_{exp,[a-z]+}.png",

    shell:
        """ ln -s {input.metric} {output.metric} && touch -h {output.metric};"""

rule plot_experiment_metrics:
    input:
        df_csv = "{experiment}/complete_pre_rec_adjrand_{exp}.csv",
    output:
        plot = "{experiment}/complete_pre_rec_adjrand_{exp}.png",
        comp = "{experiment}/pca_components_{exp}.png",
        time = "{experiment}/execution_time_{exp}.png",
        thr = "{experiment}/contigs_pass_threshold_{exp}.png",
    run:
        df = pd.read_csv(input.df_csv,header=0,dtype={"Type":str})
        df_max_ari = get_max_ari(df)

        g1,g2,g3,g4 = seaborn_plot(df_max_ari,"Dataset",plot_labels.get(wildcards.experiment))

        g1.savefig(output.plot,bbox_inches='tight')
        g2.savefig(output.time,bbox_inches='tight')
        g3.savefig(output.comp,bbox_inches='tight')
        g4.savefig(output.thr,bbox_inches='tight')


# rule plot_experiment_metrics:
#     input:
#         metric = lambda wildcards: ["{}/{}/dataset_pre_rec_adjrand_{}.csv".format(wildcards.experiment,ds, wildcards.exp) for ds in hierarchy_dict[wildcards.experiment]],
#     output:
#         plot = "{experiment}/complete_pre_rec_adjrand_{exp}.png",
#         comp = "{experiment}/pca_components_{exp}.png",
#         time = "{experiment}/execution_time_{exp}.png",
#         thr = "{experiment}/contigs_pass_threshold_{exp}.png",
#         df_csv = "{experiment}/complete_pre_rec_adjrand_{exp}.csv",
#         df_max_ari_csv = "{experiment}/complete_pre_rec_adjrand_{exp}_max_ari.csv",
#     run:
#         dfs = [pd.read_csv(f,header=0,dtype={"Type":str}) for f in sorted(input.metric,key=LooseVersion)]
#         df = pd.concat(dfs,ignore_index=True)
#         df_max_ari = get_max_ari(df)

#         g1,g2,g3,g4 = seaborn_plot(df_max_ari,"Dataset",plot_labels.get(wildcards.experiment))

#         g1.savefig(output.plot,bbox_inches='tight')
#         g2.savefig(output.time,bbox_inches='tight')
#         g3.savefig(output.comp,bbox_inches='tight')
#         g4.savefig(output.thr,bbox_inches='tight')

#         df.to_csv(output.df_csv,index=False,float_format="%.6f")
#         df_max_ari.to_csv(output.df_max_ari_csv,index=False,float_format="%.6f")


rule plot_dataset_metrics:
    input:
        metric = lambda wildcards: ["{}/{}/{}/pre_rec_adjrand_{}.csv".format(wildcards.experiment,wildcards.dataset,ty,wildcards.exp) for ty in hierarchy_dict[wildcards.experiment][wildcards.dataset]],
    output:
        plot = "{experiment}/{dataset}/dataset_pre_rec_adjrand_{exp}.png",
        comp = "{experiment}/{dataset}/dataset_pca_components_{exp}.png",
        time = "{experiment}/{dataset}/dataset_execution_time_{exp}.png",
        thr = "{experiment}/{dataset}/contigs_pass_threshold_{exp}.png",
        df_csv = "{experiment}/{dataset}/dataset_pre_rec_adjrand_{exp}.csv",
        df_max_ari_csv = "{experiment}/{dataset}/dataset_pre_rec_adjrand_{exp}_max_ari.csv",

    run:
        dfs = [pd.read_csv(f,header=0,dtype={"Type":str}) for f in sorted(input.metric,key=LooseVersion)]
        df = pd.concat(dfs,ignore_index=True)
        df_max_ari = get_max_ari(df)

        g1,g2,g3,g4 = seaborn_plot(df_max_ari,None,plot_labels.get(wildcards.experiment))

        g1.savefig(output.plot,bbox_inches='tight')
        g2.savefig(output.time,bbox_inches='tight')
        g3.savefig(output.comp,bbox_inches='tight')
        g4.savefig(output.thr,bbox_inches='tight')

        df.to_csv(output.df_csv,index=False,float_format="%.6f")
        df_max_ari.to_csv(output.df_max_ari_csv,index=False,float_format="%.6f")


rule plot_type_metrics:
    input:
        metric = lambda wildcards : ["{}/{}/{}/{}/metrics.csv".format(wildcards.experiment,wildcards.dataset,wildcards.ty,s) for s in hierarchy_dict[wildcards.experiment][wildcards.dataset][wildcards.ty]],
    output:
        df_csv = "{experiment}/{dataset}/{ty}/pre_rec_adjrand_{exp}.csv"
    run:
        dfs = [pd.read_csv(f,header=0,index_col=False,dtype={"Type":str}) for f in sorted(input.metric,key=LooseVersion)]
        df = pd.concat(dfs,ignore_index=True)
        df.to_csv(output.df_csv,index=False,float_format="%.6f")

rule metrics:
    input:
        clustering = "{experiment}/{dataset}/{type}/{seed}/clustering.csv",
        true_clust = "{dataset}-true_clustering.csv",
    output:
        metrics = "{experiment}/{dataset}/{type}/{seed}/metrics.csv",
        confusion_matrix = "{experiment}/{dataset}/{type}/{seed}/conf_matrix.csv",
        cluster_count = "{experiment}/{dataset}/{type}/{seed}/count_cluster.txt"
    params:
        dir = "{experiment}/{dataset}/{type}/{seed}/"
    run:
        true_clust = pd.read_csv(input.true_clust,index_col=0,header=None,names=["organism","ratio","ev"])
        clust = pd.read_csv(input.clustering,index_col=0,header=None,names=["clust_nr"])
        clust_comp = true_clust.join(clust)
        cluster_count = len(clust.clust_nr.unique())

        adj_rand = metrics.adjusted_rand_score(clust_comp["organism"],clust_comp["clust_nr"])

        org = sorted(clust_comp["organism"].unique())
        clust_comp["org_nr"] = clust_comp.apply(lambda row: org.index(row["organism"]),axis=1)
        conf_matrix = pd.DataFrame(metrics.confusion_matrix(clust_comp["org_nr"],clust_comp["clust_nr"]))

        cm_ptf = conf_matrix.sum(axis=0)
        cm_pt = conf_matrix.max(axis=0)
        cm_ptidx = conf_matrix.idxmax(axis=0)
        precision = cm_pt.sum() / cm_ptf.sum()

        cm_rtf = conf_matrix.sum(axis=1)
        cm_rt = conf_matrix.max(axis=1)
        cm_rtidx = conf_matrix.idxmax(axis=1)
        recall = cm_rt.sum() / cm_rtf.sum()
        met = pd.DataFrame([{"Metric":"Precision","Value":precision},
                            {"Metric":"Recall","Value":recall},
                            {"Metric":"Adjusted Rand Index","Value":adj_rand}],
                            columns=["Experiment","Dataset","Type","Seed","ClusterCount","PCA","Minutes","PassThreshold","Metric","Value"])

        pca_comp = pd.read_csv(glob("{}pca_means_gt*.csv".format(params.dir))[0],header=None).shape[1]
        start_end = pd.read_csv("{}log.txt".format(params.dir),header=None,parse_dates=[0]).iloc[(0,-1),:]
        fitting = pd.read_csv(glob("{}clustering_*.csv".format(params.dir))[0],header=None).shape[0]
        if start_end.iloc[0,1].find("Results created at") and start_end.iloc[1,1].find("CONCOCT Finished"):
            time = (start_end.iloc[1,0] - start_end.iloc[0,0]).total_seconds() / 60

        (met.Experiment,met.Dataset,met.Type,met.Seed,met.ClusterCount,met.PCA,met.Minutes,met.PassThreshold) = (wildcards.experiment,wildcards.dataset,wildcards.type,wildcards.seed,cluster_count,pca_comp,time,fitting)
        conf_matrix.to_csv(output.confusion_matrix)
        met.to_csv(output.metrics,index=False,float_format="%.6f")

rule pca_level:
    input:
        contigs = "{dataset}-Contigs.fasta",
        inputtable = "{dataset}-concoct_inputtable_4f4685e_nolen.tsv",
        true_clustering = "{dataset}-true_clustering.csv"
    output:
        "Pca/{dataset}/{percent}/{seed}/clustering.csv",
        dir = "Pca/{dataset}/{percent}/{seed}/",
    params:
        concoct="/proj/b2010008/nobackup/brynjar/hmp_new_clone/miniconda/envs/sci2/bin/concoct",
    run:
        mkdir(output.dir)
        cr = cluster_range[wildcards.dataset]
        seed(int(wildcards.seed))
        shell("""
            {params.concoct} --coverage_file {input.inputtable} --composition_file {input.contigs} -f {wildcards.seed} -c {cr} -k 2 -s --coverage_percentage_pca {wildcards.percent} --composition_percentage_pca 0 -b {output.dir}
            """)
rule covariance:
    input:
        contigs = "{dataset}-Contigs.fasta",
        inputtable = "{dataset}-concoct_inputtable_4f4685e_nolen.tsv",
        true_clustering = "{dataset}-true_clustering.csv"
    output:
        "Covariance/{dataset}/{cov}/{seed}/clustering.csv",
        dir = "Covariance/{dataset}/{cov}/{seed}/",
    params:
        concoct="/proj/b2010008/nobackup/brynjar/hmp_new_clone/miniconda/envs/sci2/bin/concoct",
    run:
        mkdir(output.dir)
        cr = cluster_range[wildcards.dataset]
        seed(int(wildcards.seed))
        shell("""
            {params.concoct} --coverage_file {input.inputtable} --composition_file {input.contigs} -f {wildcards.seed} -c {cr} -k 2 -s --composition_percentage_pca 0 -b {output.dir} --covariance_type {wildcards.cov}
            """)

rule filter_threshold:
    input:
        contigs = "{dataset}-Contigs.fasta",
        inputtable = "{dataset}-concoct_inputtable_4f4685e_nolen.tsv",
        true_clustering = "{dataset}-true_clustering.csv"
    output:
        "Threshold/{dataset}/{thr}/{seed}/clustering.csv",
        dir = "Threshold/{dataset}/{thr}/{seed}/",
    params:
        concoct="/proj/b2010008/nobackup/brynjar/hmp_new_clone/miniconda/envs/sci2/bin/concoct",
    run:
        mkdir(output.dir)
        cr = cluster_range[wildcards.dataset]
        seed(int(wildcards.seed))
        shell("""
            {params.concoct} --coverage_file {input.inputtable} --composition_file {input.contigs} -f {wildcards.seed} -c {cr} -k 2 -l {wildcards.thr} -s --composition_percentage_pca 0 -b {output.dir}
            """)
rule samples:
    input:
        contigs = "{dataset}-Contigs.fasta",
        inputtable = "Samples/{dataset}/{nr}/{dataset}-concoct_inputtable_4f4685e_nolen.tsv",
        true_clustering = "{dataset}-true_clustering.csv"
    output:
        file = "Samples/{dataset}/{nr}/{seed}/clustering.csv",
        dir = "Samples/{dataset}/{nr}/{seed}/"
    params:
        concoct="/proj/b2010008/nobackup/brynjar/hmp_new_clone/miniconda/envs/sci2/bin/concoct",
    run:
        cr = cluster_range[wildcards.dataset]
        seed(int(wildcards.seed))
        shell("""
            {params.concoct} --coverage_file {input.inputtable} --composition_file {input.contigs} -f {wildcards.seed} -c {cr} -k 2 -s --composition_percentage_pca 0 -b {output.dir}
            """)
rule final:
    input:
        contigs = "{dataset}-Contigs.fasta",
        inputtable = "{dataset}-concoct_inputtable_4f4685e_nolen.tsv",
        true_clustering = "{dataset}-true_clustering.csv"
    output:
        "Final/{dataset}/{s}/{seed}/clustering.csv",
        dir = "Final/{dataset}/{s}/{seed}",
    params:
        concoct="/proj/b2010008/nobackup/brynjar/hmp_new_clone/miniconda/envs/sci2/bin/concoct",
        length = "3000",
        cov_type = "tied",
        cov_perc = "90",
    run:
        mkdir(output.dir)
        seed(int(wildcards.seed))
        clust_range = {"Guess":{"Strain":"5,36,1","Species":"71,133,2"},"Fixed":cluster_range}
        cr = clust_range[str(wildcards.s)][str(wildcards.dataset)]
        shell("""
            {params.concoct} --coverage_file {input.inputtable} --composition_file {input.contigs} -f {wildcards.seed} -l {params.length} --covariance_type {params.cov_type} --coverage_percentage_pca {params.cov_perc} -c {cr} -k 2 -s --composition_percentage_pca 0 -b {output.dir}
            """)

rule final_composition:
    input:
        contigs = "{dataset}-Contigs.fasta",
        inputtable = "{dataset}-concoct_inputtable_4f4685e_nolen.tsv",
        true_clustering = "{dataset}-true_clustering.csv"
    output:
        "Fcomp/{dataset}/{s}/{seed}/clustering.csv",
        dir = "Fcomp/{dataset}/{s}/{seed}",
    params:
        concoct="/proj/b2010008/nobackup/brynjar/hmp_new_clone/miniconda/envs/sci2/bin/concoct",
        length = "3000",
        cov_type = "tied",
        cov_perc = "90",
    run:
        mkdir(output.dir)
        seed(int(wildcards.seed))
        clust_range = {"Guess":{"Strain":"5,36,1","Species":"71,133,2"},"Fixed":cluster_range}
        cr = clust_range[str(wildcards.s)][str(wildcards.dataset)]
        shell("""
            {params.concoct} --coverage_file {input.inputtable} --composition_file {input.contigs} -f {wildcards.seed} -l {params.length} --covariance_type {params.cov_type} --coverage_percentage_pca {params.cov_perc} -c {cr} -s --composition_percentage_pca 90 -b {output.dir}
            """)
rule dirichlet:
    input:
        contigs = "{dataset}-Contigs.fasta",
        inputtable = "{dataset}-concoct_inputtable_4f4685e_nolen.tsv",
        true_clustering = "{dataset}-true_clustering.csv"
    output:
        "Dirichlet/{dataset}/{s}/{seed}/clustering.csv",
        dir = "Dirichlet/{dataset}/{s}/{seed}",
    params:
        concoct="/proj/b2010008/nobackup/brynjar/hmp_new_clone/miniconda/envs/sci2-concoct-dirilecht/bin/concoct",
        length = "3000",
        cov_type = "tied",
        cov_perc = "90",
    run:
        mkdir(output.dir)
        seed(int(wildcards.seed))
        clust_range = {"Strain":"40,40,1","Species":"200,200,1"}
        cr = clust_range[str(wildcards.dataset)]
        shell("""
            {params.concoct} --coverage_file {input.inputtable} --composition_file {input.contigs} -f {wildcards.seed} -l {params.length} --covariance_type {params.cov_type} --coverage_percentage_pca {params.cov_perc} -c {cr} -s --composition_percentage_pca 0 -b {output.dir}
            """)

rule dpfinal_composition:
    input:
        contigs = "{dataset}-Contigs.fasta",
        inputtable = "{dataset}-concoct_inputtable_4f4685e_nolen.tsv",
        true_clustering = "{dataset}-true_clustering.csv"
    output:
        "Dcomp/{dataset}/{s}/{seed}/clustering.csv",
        dir = "Dcomp/{dataset}/{s}/{seed}",
    params:
        concoct="/proj/b2010008/nobackup/brynjar/hmp_new_clone/miniconda/envs/sci2-concoct-dirilecht/bin/concoct",
        length = "3000",
        cov_type = "tied",
        cov_perc = "90",
    run:
        mkdir(output.dir)
        seed(int(wildcards.seed))
        clust_range = {"Strain":"40,40,1","Species":"200,200,1"}
        cr = clust_range[str(wildcards.dataset)]
        shell("""
            {params.concoct} --coverage_file {input.inputtable} --composition_file {input.contigs} -f {wildcards.seed} -l {params.length} --covariance_type {params.cov_type} --coverage_percentage_pca {params.cov_perc} -c {cr} -s --composition_percentage_pca 90 -b {output.dir}
            """)

rule generate_sample_inputtable:
    input:
        inputtable = "{dataset}-concoct_inputtable_4f4685e_nolen.tsv",
    output:
        restricted_inputtable = "Samples/{dataset}/{sample}/{dataset}-concoct_inputtable_4f4685e_nolen.tsv",
        dir = "Samples/{dataset}/{sample}/"
    params:
    run:
        mkdir(output.dir)
        inputtable = pd.read_csv(input.inputtable,sep="\t",index_col=0)
        sample_cols = sorted(sample(range(len(inputtable.columns)),int(wildcards.sample)))

        ds = inputtable.iloc[:,sample_cols]
        ds.where(ds != 0,inplace=True)
        ds.to_csv(output.restricted_inputtable,sep="\t",float_format="%f",na_rep="0")

rule figures_clean:
    shell: "rm *.png all_data.csv */*.png"

rule analysis_clean:
    shell: "rm *.png all_data.csv */*.{{csv,png}} */*/*.{{csv,png}} */*/*/*.csv"

rule metrics_clean:
    shell: "rm *.png all_data.csv */*.{{csv,png}} */*/*.{{csv,png}} */*/*/*.csv */*/*/*/metrics.csv */*/*/*/conf_matrix.csv"
